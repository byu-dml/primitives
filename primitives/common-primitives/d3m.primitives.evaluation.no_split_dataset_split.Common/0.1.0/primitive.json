{
    "id": "48c683ad-da9e-48cf-b3a0-7394dba5e5d2",
    "version": "0.1.0",
    "name": "No-split tabular dataset splits",
    "python_path": "d3m.primitives.evaluation.no_split_dataset_split.Common",
    "source": {
        "name": "common-primitives",
        "contact": "mailto:mitar.commonprimitives@tnode.com",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/no_split.py",
            "https://gitlab.com/datadrivendiscovery/common-primitives.git"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/common-primitives.git@0d462f256e794155830202704b71e2aa6481c0df#egg=common_primitives"
        }
    ],
    "algorithm_types": [
        "IDENTITY_FUNCTION",
        "DATA_SPLITTING"
    ],
    "primitive_family": "EVALUATION",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "common_primitives.no_split.NoSplitDatasetSplitPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Hyperparams": "common_primitives.no_split.Hyperparams",
            "Params": "d3m.base.primitives.TabularSplitPrimitiveParams",
            "Outputs": "d3m.container.list.List",
            "Inputs": "d3m.container.list.List"
        },
        "interfaces_version": "2020.11.3",
        "interfaces": [
            "generator.GeneratorPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {},
        "arguments": {
            "hyperparams": {
                "type": "common_primitives.no_split.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.list.List",
                "kind": "PIPELINE"
            },
            "dataset": {
                "type": "d3m.container.dataset.Dataset",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "d3m.base.primitives.TabularSplitPrimitiveParams",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "This function computes everything in advance, including generating the relation graph.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "dataset",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "d3m.base.primitives.TabularSplitPrimitiveParams",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.list.List]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "For each input integer creates a ``Dataset`` split and produces the training ``Dataset`` object.\nThis ``Dataset`` object should then be used to fit (train) the pipeline.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "produce_score_data": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.list.List]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "For each input integer creates a ``Dataset`` split and produces the scoring ``Dataset`` object.\nThis ``Dataset`` object should then be used to test the pipeline and score the results.\n\nOutput ``Dataset`` objects do not have targets redacted and are not directly suitable for testing."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "dataset"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive, the ``Dataset`` to split.\n\nParameters\n----------\ndataset:\n    The dataset to split."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "dataset": "typing.Union[NoneType, d3m.container.dataset.Dataset]",
            "main_resource_id": "typing.Union[NoneType, str]",
            "splits": "typing.Union[NoneType, typing.List[typing.Tuple[numpy.ndarray, numpy.ndarray]]]",
            "graph": "typing.Union[NoneType, typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]]]"
        }
    },
    "structural_type": "common_primitives.no_split.NoSplitDatasetSplitPrimitive",
    "description": "A primitive which splits a tabular Dataset in a way that for all splits it\nproduces the same (full) Dataset. Useful for unsupervised learning tasks. .\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "ebf32b76d11a77563b5eb581e77d6ea91fec1ed06ad48bef3ae023918b4a65d0"
}
