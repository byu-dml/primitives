#!/usr/bin/env python3

import argparse
import glob
import gzip
import itertools
import json
import os.path
import sys
import traceback

import yaml

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader

D3M_PIPELINES = {
    # Core package's scoring pipeline.
    'f596cd77-25f8-4d4c-a350-bb30ab1e58f6',
}

# Allowed exceptions.
# Keep in sync with "run_validation.py".
PRIMITIVES_WITHOUT_PIPELINES = {
    # Core package primitive.
    'd3m.primitives.evaluation.compute_scores.Core',
    # Test primitives.
    'd3m.primitives.evaluation.compute_scores.Test',
    'd3m.primitives.data_generation.random.Test',
    'd3m.primitives.operator.null.UnsupervisedLearnerTest',
    'd3m.primitives.classification.random_classifier.Test',
    'd3m.primitives.regression.monomial.Test',
    'd3m.primitives.operator.null.TransformerTest',
    'd3m.primitives.operator.sum.Test',
    'd3m.primitives.operator.primitive_sum.Test',
    'd3m.primitives.operator.increment.Test',
    # Common primitives used in non-standard pipelines.
    'd3m.primitives.evaluation.fixed_split_dataset_split.Commmon',
    'd3m.primitives.evaluation.kfold_timeseries_split.Common',
    'd3m.primitives.evaluation.kfold_dataset_split.Common',
    'd3m.primitives.evaluation.redact_columns.Common',
    'd3m.primitives.evaluation.no_split_dataset_split.Common',
    'd3m.primitives.evaluation.train_score_dataset_split.Common',
    # Glue primitives
    # See https://gitlab.com/datadrivendiscovery/common-primitives/-/issues/139
    'd3m.primitives.data_transformation.flatten.DataFrameCommon',
    'd3m.primitives.data_transformation.ravel.DataFrameRowCommon',
    'd3m.primitives.data_transformation.stack_ndarray_column.Common',
    'd3m.primitives.data_transformation.dataframe_to_list.Common',
    'd3m.primitives.data_transformation.dataframe_to_ndarray.Common',
    'd3m.primitives.data_transformation.list_to_dataframe.Common',
    'd3m.primitives.data_transformation.list_to_ndarray.Common',
    'd3m.primitives.data_transformation.ndarray_to_dataframe.Common',
    'd3m.primitives.data_transformation.ndarray_to_list.Common',
    # Common primitives that need to be updated.
    # See https://gitlab.com/datadrivendiscovery/common-primitives/-/issues/137
    'd3m.primitives.data_transformation.audio_reader.Common',
    'd3m.primitives.data_transformation.csv_reader.Common',
    'd3m.primitives.data_transformation.cut_audio.Common',
    'd3m.primitives.data_transformation.video_reader.Common',
    'd3m.primitives.data_transformation.image_reader.Common',
    'd3m.primitives.data_transformation.cast_to_type.Common',
    'd3m.primitives.operator.column_map.Common',
    'd3m.primitives.data_transformation.normalize_column_references.Common',
    # See https://gitlab.com/datadrivendiscovery/common-primitives/-/issues/145
    'd3m.primitives.metalearning.metafeature_extractor.Common',
    # See https://gitlab.com/datadrivendiscovery/common-primitives/-/issues/144
    'd3m.primitives.data_transformation.label_encoder.Common',
    'd3m.primitives.data_transformation.label_decoder.Common',
}


class PipelineNotFoundError(Exception):
    pass


def primitives_used_in_pipeline(pipeline):
    primitives = set()

    for step in pipeline.get('steps', []):
        if step['type'] == 'PRIMITIVE':
            primitives.add(step['primitive']['id'])
        elif step['type'] == 'SUBPIPELINE':
            primitives.update(primitives_used_in_pipeline(step['pipeline']))
        else:
            raise ValueError(f"Unsupported pipeline step type: {step['type']}")

    return primitives


def pipelines_used_in_pipeline_run(pipeline_run):
    pipeline_ids = set()

    pipeline_ids.add(pipeline_run['pipeline']['id'])
    if 'data_preparation' in pipeline_run['run']:
        pipeline_ids.add(pipeline_run['run']['data_preparation']['pipeline']['id'])
    if 'scoring' in pipeline_run['run']:
        pipeline_ids.add(pipeline_run['run']['scoring']['pipeline']['id'])

    return pipeline_ids


def resolve_pipeline(pipelines_dir, pipeline_id):
    pipeline_path = os.path.join(pipelines_dir, '{pipeline_id}.json'.format(pipeline_id=pipeline_id))
    try:
        with open(pipeline_path, 'r', encoding='utf8') as pipeline_file:
            return json.load(pipeline_file)
    except FileNotFoundError:
        pass

    for extension in ['yml', 'yaml']:
        pipeline_path = os.path.join(pipelines_dir, '{pipeline_id}.{extension}'.format(pipeline_id=pipeline_id, extension=extension))
        try:
            with open(pipeline_path, 'r', encoding='utf8') as pipeline_file:
                return yaml.load(pipeline_file, Loader=SafeLoader)
        except FileNotFoundError:
            pass

    if pipeline_id in D3M_PIPELINES:
        return None

    raise PipelineNotFoundError


def process_directory(directory):
    known_primitives = {}
    primitives_used_in_pipelines = set()
    has_errored = False

    for primitive_annotation_path in glob.iglob('{directory}/*/*/*/primitive.json'.format(directory=directory)):
        try:
            with open(primitive_annotation_path, 'r', encoding='utf8') as primitive_annotation_file:
                primitive_annotation = json.load(primitive_annotation_file)

            if primitive_annotation['id'] in known_primitives:
                has_errored = True
                print("Error: Duplicate primitive IDs, '{first_python_path}' and '{second_python_path}'.".format(
                    first_python_path=known_primitives[primitive_annotation['id']]['python_path'],
                    second_python_path=primitive_annotation['python_path'],
                ), flush=True)
            else:
                known_primitives[primitive_annotation['id']] = primitive_annotation
        except Exception:
            print("Error at primitive '{primitive_annotation_path}'.".format(primitive_annotation_path=primitive_annotation_path), flush=True)
            traceback.print_exc()
            sys.stdout.flush()
            has_errored = True

    for pipeline_run_path in itertools.chain(
        glob.iglob('{directory}/*/*/*/pipeline_runs/*.yaml.gz'.format(directory=directory)),
        glob.iglob('{directory}/*/*/*/pipeline_runs/*.yml.gz'.format(directory=directory)),
    ):
        pipelines_dir = os.path.sep.join(pipeline_run_path.split(os.path.sep)[:-2] + ['pipelines'])

        try:
            with gzip.open(pipeline_run_path, 'rt', encoding='utf8') as pipeline_run_file:
               pipeline_runs = yaml.load_all(pipeline_run_file, Loader=SafeLoader)

               for pipeline_run in pipeline_runs:
                    for pipeline_id in pipelines_used_in_pipeline_run(pipeline_run):
                        try:
                            pipeline = resolve_pipeline(pipelines_dir, pipeline_id)
                        except PipelineNotFoundError:
                            has_errored = True
                            print("Error: Could not resolve pipeline '{pipeline_id}' in '{pipelines_dir}' for pipeline run '{pipeline_run_path}'.".format(
                                pipeline_id=pipeline_id,
                                pipelines_dir=pipelines_dir,
                                pipeline_run_path=pipeline_run_path,
                            ), flush=True)
                            continue

                        if pipeline:
                            primitives_used_in_pipelines.update(primitives_used_in_pipeline(pipeline))

        except Exception:
            print("Error at pipeline run '{pipeline_run_path}'.".format(pipeline_run_path=pipeline_run_path), flush=True)
            traceback.print_exc()
            sys.stdout.flush()
            has_errored = True

    for primitive_id, primitive in known_primitives.items():
        if primitive['python_path'] in PRIMITIVES_WITHOUT_PIPELINES:
            continue

        if primitive_id not in primitives_used_in_pipelines:
            has_errored = True
            print("Primitive is not used in any pipeline:", primitive['source']['name'], primitive['id'], primitive['python_path'], flush=True)

    return has_errored


def main():
    parser = argparse.ArgumentParser(description="Compute pipelines coverage.")
    parser.add_argument('directories', metavar='DIR', nargs='*', help="directories to compute coverage in", default=())
    arguments = parser.parse_args()

    has_errored = False
    for directory in arguments.directories:
        has_errored = process_directory(directory) or has_errored

    if has_errored:
        sys.exit(1)


if __name__ == '__main__':
    main()
