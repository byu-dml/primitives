{
    "algorithm_types": [
        "SUPPORT_VECTOR_MACHINE"
    ],
    "name": "lupi_svm.LupiSvmClassifier",
    "description": "This is an implementation of the LUPISVM primitive. This is the sklearn implementation of the LUPI primitive,\nwhich can solve the *same* binary and multiclass classification tasks as standard sklearn.svm, except that privileged\ninformation (i.e., additional features available for training dataset, but absent for test dataset) is present.\nThe training dataset should provide a list of the indices indicating the features are privileged.\nThe test data (or the unlabeled data to predict) should not have the privileged features.\nThe values of the features in dataset need to be numerical, the label classes to be predicted should be categorical,\nand the missing values need to be imputed. The current implementation solves binary classification tasks.\nFuture versions will also solve multiclass classification tasks.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "primitive_family": "CLASSIFICATION",
    "python_path": "d3m.primitives.classification.lupi_svm.LupiSvmClassifier",
    "source": {
        "name": "VencoreLabs",
        "contact": "mailto:plin@perspectalabs.com",
        "uris": [
            "https://gitlab.com/d3m-perspectalabs-primitives/lupi_primitive/blob/master/lupi_svm/lupisvm.py",
            "https://gitlab.com/d3m-perspectalabs-primitives/lupi_primitive.git"
        ]
    },
    "version": "v6.0.0",
    "id": "3fd60679-cc91-3703-9ce5-34098babbbdc",
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://gitlab.com/d3m-perspectalabs-primitives/lupi_primitive.git@1a2f8e7a143760ca734c6dee7558ae1c7af9abcd#egg=lupi_svm"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "lupi_svm.lupisvm.LupiSvmClassifier",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "lupi_svm.lupisvm.Params",
            "Hyperparams": "lupi_svm.lupisvm.Hyperparams"
        },
        "interfaces_version": "2020.1.9",
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "C": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 1,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Penalty parameter C of the error term.",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "kernel": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "rbf",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Specifies the kernel type to be used in the algorithm. It must be one of 'linear', 'poly', 'rbf', 'sigmoid', 'precomputed' or a callable. If none is given, 'rbf' will be used.",
                "values": [
                    "linear",
                    "poly",
                    "rbf",
                    "sigmoid",
                    "precomputed"
                ]
            },
            "cv_score": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "accuracy",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Specifies the scoring for cross validation.",
                "values": [
                    "f1",
                    "accuracy"
                ]
            },
            "degree": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 3,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Degree of the polynomial kernel function ('poly'). Ignored by all other kernels.",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "gamma": {
                "type": "d3m.metadata.hyperparams.Union",
                "default": "auto",
                "structural_type": "typing.Union[float, str]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Kernel coefficient for 'rbf', 'poly' and 'sigmoid'. If gamma is 'auto' then 1/n_features will be used instead.  coef0 : float, optional (default=0.0) Independent term in kernel function. It is only significant in 'poly' and 'sigmoid'.",
                "configuration": {
                    "float": {
                        "type": "d3m.metadata.hyperparams.Bounded",
                        "default": 0.1,
                        "structural_type": "float",
                        "semantic_types": [
                            "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                        ],
                        "lower": 0,
                        "upper": null,
                        "lower_inclusive": true,
                        "upper_inclusive": false
                    },
                    "auto": {
                        "type": "d3m.metadata.hyperparams.Constant",
                        "default": "auto",
                        "structural_type": "str",
                        "semantic_types": [
                            "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                        ],
                        "description": "1/n_features will be used."
                    }
                }
            },
            "coef0": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ]
            },
            "probability": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Whether to enable probability estimates. This must be enabled prior to calling `fit`, and will slow down that method. "
            },
            "shrinking": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Whether to use the shrinking heuristic. "
            },
            "tol": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0.001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Tolerance for stopping criterion.",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "class_weight": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": "balanced",
                "structural_type": "typing.Union[dict, str]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Set the parameter C of class i to class_weight[i]*C for SVC. If not given, all classes are supposed to have weight one. The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))`` "
            },
            "max_iter": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": -1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Hard limit on iterations within solver, or -1 for no limit. "
            },
            "n_jobs": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 4,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Number of jobs to run in parallel. "
            },
            "gamma_gridsearch": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": [
                    -1.0,
                    6.5,
                    3.4
                ],
                "structural_type": "typing.Tuple[float, float, float]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Specifiy the range and step for the primitive internal grid search on gamma parameter. Expecting a tuple (lower bound, upper bound, step)"
            },
            "C_gridsearch": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": [
                    -1.0,
                    6.5,
                    3.4
                ],
                "structural_type": "typing.Tuple[float, float, float]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Specifiy the range and step for the primitive internal grid search on C parameter. Expecting a tuple (lower bound, upper bound, step)"
            },
            "return_result": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "new",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
                "values": [
                    "append",
                    "replace",
                    "new"
                ]
            },
            "use_semantic_types": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
            },
            "add_index_columns": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\"."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "lupi_svm.lupisvm.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "docker_containers": {
                "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "lupi_svm.lupisvm.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "docker_containers"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to fully refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "lupi_svm.lupisvm.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs : Inputs\n    The inputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "support_": "typing.Union[NoneType, numpy.ndarray]",
            "support_vectors_": "typing.Union[NoneType, numpy.ndarray]",
            "n_support_": "typing.Union[NoneType, numpy.ndarray]",
            "dual_coef_": "typing.Union[NoneType, numpy.ndarray]",
            "coef_": "typing.Union[NoneType, numpy.ndarray]",
            "intercept_": "typing.Union[NoneType, numpy.ndarray]",
            "_sparse": "typing.Union[NoneType, bool]",
            "shape_fit_": "typing.Union[NoneType, tuple]",
            "_dual_coef_": "typing.Union[NoneType, numpy.ndarray]",
            "_intercept_": "typing.Union[NoneType, numpy.ndarray]",
            "probA_": "typing.Union[NoneType, numpy.ndarray]",
            "probB_": "typing.Union[NoneType, numpy.ndarray]",
            "_gamma": "typing.Union[NoneType, float]",
            "classes_": "typing.Union[NoneType, numpy.ndarray]",
            "target_names_": "typing.Union[NoneType, typing.Sequence[typing.Any]]"
        }
    },
    "structural_type": "lupi_svm.lupisvm.LupiSvmClassifier",
    "digest": "ed33589a1f9539d08affa2e2e09fd5d86dddd0e657f8a6a43d9d92f8906c624c"
}
