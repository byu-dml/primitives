{
  "id": "a184a1d1-3187-4d1f-99c6-e1d5665c2c99",
  "version": "0.1.4",
  "name": "MFCC Feature Extraction",
  "description": "BBN D3M Signal Mel-Frequency Cepstral Coefficient (MFCC) Extraction Primitive extracts feature vectors of Mel-frequency Cepstral Coefficients (MFCCs) for frames of audio.\nInput: List of arrays with frames of shape [ num_frames, frame_length ]\nOutput: List of arrays with feature vectors extracted for frames [ num_frames, num_features ]\nApplications include: audio, time-series classification\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
  "keywords": [],
  "source": {
    "name": "BBN",
    "contact": "mailto:prasannakumar.muthukumar@raytheon.com",
    "uris": [
      "https://github.com/BBN-E/d3m-bbn-primitives/blob/a8e9a2888b691db93ada09a785a166624d92fd34/bbn_primitives/time_series/signal_mfcc.py",
      "https://github.com/BBN-E/d3m-bbn-primitives.git"
    ]
  },
  "installation": [
    {
      "type": "PIP",
      "package_uri": "git+https://github.com/BBN-E/d3m-bbn-primitives.git@a8e9a2888b691db93ada09a785a166624d92fd34#egg=bbn_primitives"
    }
  ],
  "python_path": "d3m.primitives.feature_extraction.signal_mfcc.SignalMFCC",
  "algorithm_types": [
    "MFCC_FEATURE_EXTRACTION"
  ],
  "primitive_family": "FEATURE_EXTRACTION",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
  "original_python_path": "bbn_primitives.time_series.signal_mfcc.SignalMFCC",
  "primitive_code": {
    "class_type_arguments": {
      "Inputs": "d3m.container.list.List",
      "Outputs": "d3m.container.list.List",
      "Hyperparams": "bbn_primitives.time_series.signal_mfcc.Hyperparams",
      "Params": "NoneType"
    },
    "interfaces_version": "2019.5.8",
    "interfaces": [
      "featurization.FeaturizationTransformerPrimitiveBase",
      "transformer.TransformerPrimitiveBase",
      "base.PrimitiveBase"
    ],
    "hyperparams": {
      "frame_mean_norm": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Frame mean normalization"
      },
      "preemcoef": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": null,
        "structural_type": "typing.Union[NoneType, float]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "preemphasis"
      },
      "nfft": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": null,
        "structural_type": "typing.Union[NoneType, int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "FFT size"
      },
      "num_chans": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 20,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Number of filter banks"
      },
      "num_ceps": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 12,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Number of cepstral coefficients (output dimensionality)"
      },
      "use_power": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Use power"
      },
      "cep_lifter": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 22.0,
        "structural_type": "float",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Cepstrum lifter weight"
      }
    },
    "arguments": {
      "hyperparams": {
        "type": "bbn_primitives.time_series.signal_mfcc.Hyperparams",
        "kind": "RUNTIME"
      },
      "random_seed": {
        "type": "int",
        "kind": "RUNTIME",
        "default": 0
      },
      "docker_containers": {
        "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
        "kind": "RUNTIME",
        "default": null
      },
      "timeout": {
        "type": "typing.Union[NoneType, float]",
        "kind": "RUNTIME",
        "default": null
      },
      "iterations": {
        "type": "typing.Union[NoneType, int]",
        "kind": "RUNTIME",
        "default": null
      },
      "produce_methods": {
        "type": "typing.Sequence[str]",
        "kind": "RUNTIME"
      },
      "inputs": {
        "type": "d3m.container.list.List",
        "kind": "PIPELINE"
      },
      "params": {
        "type": "NoneType",
        "kind": "RUNTIME"
      }
    },
    "class_methods": {
      "can_accept": {
        "arguments": {
          "method_name": {
            "type": "str"
          },
          "arguments": {
            "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
          },
          "hyperparams": {
            "type": "bbn_primitives.time_series.signal_mfcc.Hyperparams"
          }
        },
        "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
        "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
      }
    },
    "instance_methods": {
      "__init__": {
        "kind": "OTHER",
        "arguments": [
          "hyperparams",
          "random_seed",
          "docker_containers"
        ],
        "returns": "NoneType"
      },
      "fit": {
        "kind": "OTHER",
        "arguments": [
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
        "description": "A noop.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
      },
      "fit_multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "get_params": {
        "kind": "OTHER",
        "arguments": [],
        "returns": "NoneType",
        "description": "A noop.\n\nReturns\n-------\nParams\n    An instance of parameters."
      },
      "multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "produce": {
        "kind": "PRODUCE",
        "arguments": [
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.list.List]",
        "singleton": false,
        "inputs_across_samples": [],
        "description": "Arguments:\n    - inputs: [ num_windows, window_len ]\n\nReturns:\n    - [ num_windows, nfft ]\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
      },
      "set_params": {
        "kind": "OTHER",
        "arguments": [
          "params"
        ],
        "returns": "NoneType",
        "description": "A noop.\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
      },
      "set_training_data": {
        "kind": "OTHER",
        "arguments": [],
        "returns": "NoneType",
        "description": "A noop.\n\nParameters\n----------"
      }
    },
    "class_attributes": {
      "logger": "logging.Logger",
      "metadata": "d3m.metadata.base.PrimitiveMetadata"
    },
    "instance_attributes": {
      "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
      "random_seed": "int",
      "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
      "volumes": "typing.Dict[str, str]",
      "temporary_directory": "typing.Union[NoneType, str]"
    }
  },
  "structural_type": "bbn_primitives.time_series.signal_mfcc.SignalMFCC",
  "digest": "71110d94bf9cf25dcf14bbf87ddae15a97f17f7b73862f233a236710c444b70e"
}
